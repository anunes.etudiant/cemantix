package com.ensiie.dmia.cemantix.profil
import java.io.Serializable
import java.util.UUID

@kotlinx.serialization.Serializable
data class Profile (
    @kotlinx.serialization.SerialName("id")
    val id : String = UUID.randomUUID().toString(),

    @kotlinx.serialization.SerialName("username")
    val username : String = "",

    @kotlinx.serialization.SerialName("words_found")
    var wordsFound : Int = 0,

    @kotlinx.serialization.SerialName("total_attempts")
    var totalAttemps : Int = 0,

    @kotlinx.serialization.SerialName("min_attempts")
    var minAttempts : Int = 0,

    @kotlinx.serialization.SerialName("days_won")
    var daysWon : List<Int> = emptyList()

):Serializable
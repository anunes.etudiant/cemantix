package com.ensiie.dmia.cemantix.guesslist

import android.animation.ObjectAnimator
import android.media.MediaPlayer
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ensiie.dmia.cemantix.R
import android.view.View;
import android.view.animation.LinearInterpolator
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentContainerView


class GuessViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {

    fun bind(guess: Guess?) {

        val count = itemView.findViewById<TextView>(R.id.count);
        val error = itemView.findViewById<TextView>(R.id.guess_error);
        val word = itemView.findViewById<TextView>(R.id.guess_word);
        val score = itemView.findViewById<TextView>(R.id.guess_score);
        val tempIcon = itemView.findViewById<TextView>(R.id.guess_temp_icon);
        val percentile = itemView.findViewById<TextView>(R.id.guess_percentile);
        val percentileBar = itemView.findViewById<ProgressBar>(R.id.guess_percentile_bar)


        count.text = "";
        error.text = "";
        word.text = "";
        score.text = "";
        tempIcon.text = "";
        percentile.text = "";
        percentileBar.visibility = View.INVISIBLE;

        if(guess == null) return;


        if(guess.score != null){

            count.text = guess.count.toString();
            word.text = guess.word;
            score.text = "%.1f".format(guess.score.times(100));
            tempIcon.text = getMood(guess);
            if(guess.percentile != null){
                percentile.text = guess.percentile.toString();
                loadProgress(percentileBar, guess.percentile);
            }
        }else{
            error.text = "Je ne connais pas le mot '"+guess.word+"'";
        }
    }

    private fun getMood(guess: Guess) : String{
        var mood : String = "🧊";
        if(guess.percentile != null){
            if (guess.percentile == 1000) {
                mood = "🥳";
            } else if (guess.percentile == 999)
                mood = "😱";
            else if (guess.percentile >= 990)
                mood = "🔥";
            else if (guess.percentile >= 900)
                mood = "🥵";
            else
                mood = "😎";
        }else if (guess.score != null && guess.score >= 0){
            mood = "🥶";
        }
        return mood;
    }


    fun loadProgress(progressBar : ProgressBar,percentile : Int){
        val oui_sound = MediaPlayer.create(progressBar.context, R.raw.ouiiii);
        oui_sound.start()
        val animator = ObjectAnimator.ofInt(progressBar, "progress", 0, percentile.div(10))
        animator.duration = 1000
        animator.interpolator = LinearInterpolator()
        animator.start()
        progressBar.visibility = View.VISIBLE;
    }


    fun internetError(){
        val count = itemView.findViewById<TextView>(R.id.count);
        val error = itemView.findViewById<TextView>(R.id.guess_error);
        val word = itemView.findViewById<TextView>(R.id.guess_word);
        val score = itemView.findViewById<TextView>(R.id.guess_score);
        val tempIcon = itemView.findViewById<TextView>(R.id.guess_temp_icon);
        val percentile = itemView.findViewById<TextView>(R.id.guess_percentile);
        val percentileBar = itemView.findViewById<ProgressBar>(R.id.guess_percentile_bar)


        count.text = "";
        error.text = "Pas de connexion internet !";
        word.text = "";
        score.text = "";
        tempIcon.text = "";
        percentile.text = "";
        percentileBar.visibility = View.INVISIBLE;
    }



}

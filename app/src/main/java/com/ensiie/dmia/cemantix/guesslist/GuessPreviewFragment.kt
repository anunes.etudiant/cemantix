package com.ensiie.dmia.cemantix.guesslist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.ensiie.dmia.cemantix.data.Api
import com.ensiie.dmia.cemantix.databinding.FragmentGuessListBinding
import com.ensiie.dmia.cemantix.databinding.ItemGuessBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * A simple [Fragment] subclass.
 * Use the [GuessListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GuessPreviewFragment : Fragment() {

    private lateinit var binding : ItemGuessBinding;
    private lateinit var initCallback : () -> Unit;
    lateinit var guessViewHolder: GuessViewHolder


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ItemGuessBinding.inflate(inflater, container, false);
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        guessViewHolder = GuessViewHolder(view)
        initCallback();
    }

    fun afterViewCreated(callback : () -> Unit){
        initCallback = callback;
    }

}
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import coil.load
import com.ensiie.dmia.cemantix.R
import com.ensiie.dmia.cemantix.databinding.FragmentHeaderBinding
import com.ensiie.dmia.cemantix.guesslist.GuessListFragment
import com.ensiie.dmia.cemantix.profil.EditProfile
import com.ensiie.dmia.cemantix.profil.Profile
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.Instant
import java.time.LocalDate

/**
 * A simple [Fragment] subclass.
 * Use the [GuessListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HeaderFragment : Fragment() {

    private lateinit var binding : FragmentHeaderBinding;



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHeaderBinding.inflate(inflater, container, false);


        val profile = getCurrentOrCreateProfile();
        loadProfilePic(profile.id);

        val openProfile = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
            val profile : Profile? = result.data?.getSerializableExtra("profile") as? Profile
            if(profile != null){
                loadProfilePic(profile.id);
            }

        }
        val intentProfile = Intent(context, EditProfile::class.java)

        val place_sound = MediaPlayer.create(context, R.raw.place);


        binding.profilPic.setOnClickListener{openProfile.launch(intentProfile)
        place_sound.start()}

        val openLink = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){}
        val uri: Uri = Uri.parse("https://cemantix.certitudes.org/")
        val intentLink = Intent(Intent.ACTION_VIEW, uri)
        binding.logo.setOnClickListener{openLink.launch(intentLink)}


        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.day.text = "Jour n°"+getDay();
        binding.people.text = "";
    }

    private fun loadProfilePic(profileID: String){
        binding.profilPic.load("https://nunesa.iiens.net/cemantix/avatar/"+profileID+"?t="+ Instant.now().epochSecond.toString())
    }

    fun updateSolvers(solvers : Int){
        binding.people.text = "Trouvé par "+ solvers+" personnes"
    }

    private fun getCurrentOrCreateProfile() : Profile {
        val sharedPreferences = context?.getSharedPreferences("cemantix", Context.MODE_PRIVATE);
        val editor = sharedPreferences?.edit()

        val json =  sharedPreferences?.getString("profile", "{}")!!;
        val profile = Json.decodeFromString<Profile>(json);

        editor?.putString("profile", Json.encodeToString<Profile>(profile));
        editor?.apply();

        return profile;
    }


    fun bingo(c : Int){
        val bravo_sound = MediaPlayer.create(context, R.raw.bravo);
        bravo_sound.start()


        if(context != null) {
            binding.root.setBackgroundColor(context!!.getColor(R.color.cem_green));
        }

        binding.day.text = "Bravo !!! Trouvé en " + c + " coups";

    }

    fun clearBingo(){
        binding.root.setBackgroundColor(context!!.getColor(R.color.ic_launcher_background));
        binding.day.text = "Jour n°"+getDay();
    }

    fun getDay() : Int{
        val now = LocalDate.now();
        return  (now.dayOfYear + now.year) - 1719
    }



}
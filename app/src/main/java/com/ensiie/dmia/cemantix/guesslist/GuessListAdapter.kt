package com.ensiie.dmia.cemantix.guesslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ensiie.dmia.cemantix.R

object GuessDiffCallback : DiffUtil.ItemCallback<Guess>() {
    override fun areItemsTheSame(oldItem: Guess, newItem: Guess) : Boolean {
        return oldItem.word == newItem.word;
    }

    override fun areContentsTheSame(oldItem: Guess, newItem: Guess) : Boolean {
        return oldItem == newItem;
    }
}

class GuessListAdapter : ListAdapter<Guess, GuessViewHolder>(GuessDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuessViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_guess, parent, false);
        return GuessViewHolder(itemView);
    }

    override fun onBindViewHolder(holder: GuessViewHolder, position: Int) {
        holder.bind(currentList[position]);
    }

}
@file:OptIn(ExperimentalTextApi::class)

package com.ensiie.dmia.cemantix.profil.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.googlefonts.Font
import androidx.compose.ui.text.googlefonts.GoogleFont
import androidx.compose.ui.unit.sp
import com.ensiie.dmia.cemantix.R

// Set of Material typography styles to start with
val Typography = Typography(body1 = TextStyle(
    fontFamily = FontFamily(androidx.compose.ui.text.font.Font(R.font.alata_font)),
    fontWeight = FontWeight.Normal,
    fontSize = 18.sp
)


    /*defaultFontFamily = getGoogleFontFamily(
        name = "Alata",
        weights = listOf(
            FontWeight.Normal,
            FontWeight.Bold,
            FontWeight.ExtraLight,
            FontWeight.SemiBold
        )
    )*/
    /* Other default text styles to override
    button = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.W500,
        fontSize = 14.sp
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
    */
)


/*private fun getGoogleFontFamily(
    name: String,
    weights: List<FontWeight>
): FontFamily {
    return FontFamily(
        weights.map {
            Font(GoogleFont(name), googleFontProvider, it)
        }
    )
}

private val googleFontProvider =
    GoogleFont.Provider(
        providerAuthority = "com.google.android.gms.fonts",
        providerPackage = "com.google.android.gms",
        certificates = R.array.com_google_android_gms_fonts_certs
    )



val AppFontTypography = Typography(

)*/
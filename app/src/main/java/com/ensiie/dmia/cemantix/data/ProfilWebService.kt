package com.ensiie.dmia.cemantix.data


import com.ensiie.dmia.cemantix.profil.Profile
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ProfileWebService {

    @Multipart
    @POST("/cemantix/avatar")
    suspend fun updateAvatar(@Part avatar: MultipartBody.Part): Response<Profile>
}
package com.ensiie.dmia.cemantix.data

import android.content.Context
import com.ensiie.dmia.cemantix.guesslist.Guess
import com.ensiie.dmia.cemantix.profil.Profile
import kotlinx.serialization.json.Json
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import okhttp3.internal.cache2.Relay.Companion.edit
import java.time.LocalDate
import java.util.*
import kotlinx.serialization.decodeFromString

class ProfileStorage (val context: Context){

    val now = LocalDate.now();

    fun getDay() : Int{
        val now = LocalDate.now();
        return  (now.dayOfYear + now.year) - 1719
    }

    fun loadProfile() : Profile {
        val sharedPreferences = context.getSharedPreferences("cemantix", Context.MODE_PRIVATE);
        val json =  sharedPreferences.getString("profile", "{}")!!;
        val profile = Json.decodeFromString<Profile>(json);
        saveProfile(profile)

        return profile;
    }


    fun saveProfile(profile : Profile) {
        val sharedPreferences = context.getSharedPreferences("cemantix", Context.MODE_PRIVATE);
        val editor = sharedPreferences.edit()

        editor.putString("profile", Json.encodeToString<Profile>(profile));
        editor.apply();
    }


    fun incrAttempts() {
        val profile = loadProfile()
        profile.totalAttemps++;
        saveProfile(profile)
    }

    fun foundWord(tries : Int) {
        val profile = loadProfile()
        val day = getDay()
        if (!(day in profile.daysWon)) {
            profile.daysWon = profile.daysWon + day
            profile.wordsFound++;
            if (profile.minAttempts == 0 || tries < profile.minAttempts)
                profile.minAttempts = tries;

            saveProfile(profile)
        }

    }

}
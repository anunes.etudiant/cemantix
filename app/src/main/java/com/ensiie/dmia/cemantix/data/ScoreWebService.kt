package com.ensiie.dmia.cemantix.data

import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ScoreWebService {

    @FormUrlEncoded
    @POST("/score")
    suspend fun fetchScore(@Field("word") word: String): Response<ScoreApiResult>
}
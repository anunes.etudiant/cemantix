package com.ensiie.dmia.cemantix.profil

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.MediaRecorder.AudioSource
import android.media.SoundPool
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.ensiie.dmia.cemantix.R
import com.ensiie.dmia.cemantix.data.Api
import com.ensiie.dmia.cemantix.profil.ui.theme.CemantixTheme
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File


class EditProfile : ComponentActivity() {

    val fontTest = FontFamily(Font(R.font.alata, FontWeight.Normal))


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CemantixTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    //color = MaterialTheme.colors.background
                    color = Color.Black
                ) {
                    ProfileDetail(getCurrentProfil()) { onSave(it) };
                }
            }
        }
    }

    fun getCurrentProfil() : Profile{
        return Json.decodeFromString<Profile>(getSharedPreferences("cemantix", Context.MODE_PRIVATE).getString("profile", "{}")!!);
    }

    fun onSave(newProfile : Profile){

        val sharedPreferences = getSharedPreferences("cemantix", Context.MODE_PRIVATE)

        val editor = sharedPreferences.edit()
        editor.putString("profile", Json.encodeToString<Profile>(newProfile));
        editor.apply();

        intent.putExtra("profile", newProfile);
        setResult(RESULT_OK, intent);

        finish();

    }

    private fun Uri.toRequestBody(uuid: String): MultipartBody.Part {
        val fileInputStream = contentResolver.openInputStream(this)!!
        val fileBody = fileInputStream.readBytes().toRequestBody()
        return MultipartBody.Part.createFormData(
            name = "avatar",
            filename = uuid,
            body = fileBody
        )
    }


    @Composable
    fun ProfileDetail(currentProfile: Profile, onValidate: (Profile) -> Unit) {

        val composeScope = rememberCoroutineScope()
        var picHasChanged by remember { mutableStateOf(false) };
        var profile by remember { mutableStateOf(currentProfile) }
        var uri: Uri? by remember { mutableStateOf(Uri.parse("https://nunesa.iiens.net/cemantix/avatar/"+currentProfile.id)) }

        val captureUri by lazy {
            contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, ContentValues())
        }

        val oulah_sound = MediaPlayer.create(this, R.raw.oulah);

        val takePicture = rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) { success->
            if(success){
                uri = captureUri;
                picHasChanged = true;
                oulah_sound.start()
            }
        }

        val pickPicture =
            rememberLauncherForActivityResult(ActivityResultContracts.PickVisualMedia()) {
                uri = it;
                picHasChanged = true;
                oulah_sound.start()
            }

        val requestPermPicAndLaunch =  rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) {
            pickPicture.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        val tut_sound = MediaPlayer.create(this, R.raw.tut);
        val validation_sound = MediaPlayer.create(this, R.raw.validation);
        val les_photos_sound = MediaPlayer.create(this, R.raw.les_photos);
        val joli_sound = MediaPlayer.create(this, R.raw.joli_par_ici);


        // Display
        Column(
            modifier = Modifier.padding(Dp(8.0f)),
            verticalArrangement = Arrangement.spacedBy(Dp(16.0f))
        ) {
            Row(
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween

            ) {

                // Bouton pour annuler
                Button(onClick = { finish()
                                 tut_sound.start()
                                 },
                    colors = ButtonDefaults.buttonColors(Color.Transparent),
                    shape = RoundedCornerShape(48.dp),
                    border = BorderStroke(0.dp, Color.Transparent))
                {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.icon_cancel_32),
                        contentDescription = "drawable icons",
                        tint = Color.Red
                    )
                }

                // Profil (texte)
                Text(
                    text = "Modifier profil",
                    style = MaterialTheme.typography.h1.copy(fontSize = 24.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White
                )

                // Bouton pour valider
                Button(onClick = {validation_sound.start()
                    if(uri != null && picHasChanged) {
                    composeScope.launch {
                        try {
                            Api.profilWebService.updateAvatar(uri!!.toRequestBody(profile.id))
                        }catch(e : Exception){
                        }
                            onValidate(profile);

                    }
                } else{
                    onValidate(profile);
                }
                },
                    colors = ButtonDefaults.buttonColors(Color.Transparent),
                    shape = RoundedCornerShape(48.dp),
                    border = BorderStroke(0.dp, Color.Transparent)) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.icon_validate_32),
                        contentDescription = "drawable icons",
                        tint = Color.Green
                    )
                }


            }

            Row(
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                horizontalArrangement = Arrangement.Center

            ) {
                // Photo de profil
                AsyncImage(
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.size(128.dp).clip(CircleShape),
                    model = uri,
                    contentDescription = null
                )
            }



            Row(
                modifier = Modifier.fillMaxWidth().padding(horizontal = 96.dp),
                horizontalArrangement = Arrangement.SpaceBetween

            ) {
                // Bouton pour prendre une photo
                Button(
                    onClick = { takePicture.launch(captureUri)
                              joli_sound.start()},
                    shape = RoundedCornerShape(48.dp),
                    colors = ButtonDefaults.buttonColors(Color(210, 50, 50)),
                    //modifier = Modifier.background(Brush.horizontalGradient(0f to Color.Red, 1000f to Color.Magenta))
                    //colors = ButtonDefaults.buttonColors(Color.Black)
                ) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.icon_photo_32),
                        contentDescription = "drawable icons",
                        tint = Color.White
                    )
                }

                // Bouton pour utiliser une photo de la galerie
                Button(
                    onClick = { requestPermPicAndLaunch.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                              les_photos_sound.start()},
                    shape = RoundedCornerShape(48.dp),
                    colors = ButtonDefaults.buttonColors(Color(50, 50, 210))
                ) {
                    Icon(
                        imageVector = ImageVector.vectorResource(id = R.drawable.icon_gallery_32),
                        contentDescription = "drawable icons",
                        tint = Color.White
                    )
                }
            }



            // Pseudo
            OutlinedTextField(value = profile.username,
                onValueChange = { profile = profile.copy(username = it) },
                label = { Text("Pseudo", color = Color.White, fontFamily = FontFamily(Font(R.font.alata_font))) },
                modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = Color.White,
                    unfocusedBorderColor = Color.Gray,
                    textColor = Color.White,
                    cursorColor = Color.White)
            )

            // Pour le debug
            //Text(text = profile.id, fontFamily = FontFamily(Font(R.font.alata_font)));

            Divider(color = Color.Gray, thickness = 1.dp)


            // Statistiques personalisées
            Text(text = "Statistiques",
                style = MaterialTheme.typography.h1.copy(fontSize = 24.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                color = Color.White);

            Row(
                modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(text = "Nombre de mots trouvés",
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
                Text(text = profile.wordsFound.toString(),
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
            }

            Row(
                modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(text = "Nombre d'essais totaux",
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
                Text(text = profile.totalAttemps.toString(),
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
            }

            Row(
                modifier = Modifier.fillMaxWidth().padding(horizontal = 16.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(text = "Nombre d'essais minimum",
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
                Text(text = profile.minAttempts.toString(),
                    style = MaterialTheme.typography.h1.copy(fontSize = 18.sp, fontFamily = FontFamily(Font(R.font.alata_font))),
                    color = Color.White);
            }

        }

    }

    @Preview(showBackground = true)
    @Composable
    fun ProfilPreview() {
        CemantixTheme {
            ProfileDetail(Profile("1", "Michel"), {})
        }
    }

}










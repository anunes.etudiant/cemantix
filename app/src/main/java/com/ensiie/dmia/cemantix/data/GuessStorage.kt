package com.ensiie.dmia.cemantix.data

import android.content.Context
import com.ensiie.dmia.cemantix.guesslist.Guess
import kotlinx.serialization.json.Json
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import okhttp3.internal.cache2.Relay.Companion.edit
import java.time.LocalDate
import java.util.*
import kotlinx.serialization.decodeFromString

class GuessStorage (val context: Context){

    fun updateDate() : Boolean{
        val now = LocalDate.now();
        val day =  (now.dayOfYear + now.year).toString()
        val sharedPreferences = context.getSharedPreferences("cemantix", Context.MODE_PRIVATE);

        val previousSavedDay = sharedPreferences.getString("day", "")!!;

        if(day != previousSavedDay){
            val editor = sharedPreferences.edit();
            editor.putString("day", day);
            editor.apply();

            if(previousSavedDay.isNotEmpty()) return true;
        }

        return false;
    }

    fun loadGuesses() : List<Guess> {
        if(updateDate()) return listOf();

        val sharedPreferences = context.getSharedPreferences("cemantix", Context.MODE_PRIVATE);
        return Json.decodeFromString<List<Guess>>(sharedPreferences.getString("guesses", "[]")!!);
    }


    fun saveGuesses(newList : List<Guess>) : Boolean{

        if(updateDate()) return false;

        val sharedPreferences = context.getSharedPreferences("cemantix", Context.MODE_PRIVATE);
        val editor = sharedPreferences.edit();

        editor.putString("guesses", Json.encodeToString<List<Guess>>(newList));
        editor.apply();

        return true;

    }

}
package com.ensiie.dmia.cemantix.guesslist

import java.io.Serializable


@kotlinx.serialization.Serializable
data class Guess(

    @kotlinx.serialization.SerialName("word")
    val word : String = "",

    @kotlinx.serialization.SerialName("score")
    val score : Float? = null,

    @kotlinx.serialization.SerialName("percentile")
    val percentile : Int? = null,

    @kotlinx.serialization.SerialName("count")
    var count : Int = 0,

    ): Serializable

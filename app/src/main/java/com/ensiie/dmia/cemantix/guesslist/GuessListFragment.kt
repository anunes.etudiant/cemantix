package com.ensiie.dmia.cemantix.guesslist

import HeaderFragment
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.text.method.TextKeyListener.clear
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.ensiie.dmia.cemantix.R
import com.ensiie.dmia.cemantix.data.Api
import com.ensiie.dmia.cemantix.data.GuessStorage
import com.ensiie.dmia.cemantix.data.ProfileStorage
import com.ensiie.dmia.cemantix.databinding.FragmentGuessListBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString

/**
 * A simple [Fragment] subclass.
 * Use the [GuessListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GuessListFragment : Fragment() {

    private lateinit var binding : FragmentGuessListBinding;
    private val viewModel: GuessListViewModel by viewModels()

    private val adapter = GuessListAdapter();

    var bing = 0;


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentGuessListBinding.inflate(inflater, container, false);
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fragmentContainerView.getFragment<GuessPreviewFragment>().afterViewCreated {
            initFragment();
        }
    }

    fun initFragment(){
        val guessStorage = GuessStorage(context!!);
        val profileStorage = ProfileStorage(context!!);

        val recyclerView = binding.recyclerView;
        val wordInput = binding.wordInput;

        recyclerView.adapter = adapter;

        val list = guessStorage.loadGuesses()
        val guess : Guess ? = list.find { g -> g.percentile != null && g.percentile == 1000 }

        if (guess != null) {
            bing = guess.count;
            binding.fragmentContainerView2.getFragment<HeaderFragment>().bingo(guess.count);
        }

        viewModel.onInternetError = {
            Toast.makeText(context, "Pas de connexion à internet !", Toast.LENGTH_LONG).show();
            binding.fragmentContainerView.getFragment<GuessPreviewFragment>().guessViewHolder.internetError();
        }

        viewModel.submitGuessList(list)


        val clic_sound = MediaPlayer.create(context, R.raw.clic);
        val tut_sound = MediaPlayer.create(context, R.raw.tut);
        val bravo_sound = MediaPlayer.create(context, R.raw.bravo);


        binding.validateWord.setOnClickListener{
            validateWord();
        }

        binding.cleanList.setOnClickListener{
            clear();
        }


        lifecycleScope.launch {
            viewModel.guessStateFlow.collect {newList ->
                if(newList != null) {
                    if (guessStorage.saveGuesses(newList)) {
                        adapter.submitList(newList);
                    }else{
                        clear();
                    }
                    binding.recyclerView.scrollToPosition(0);
                }
            }
        }


        lifecycleScope.launch {
            viewModel.lastGuessStateFlow.collect { newGuess ->
                binding.fragmentContainerView.getFragment<GuessPreviewFragment>().guessViewHolder.bind(
                    newGuess
                )
                if(newGuess != null) {

                    binding.fragmentContainerView.getFragment<GuessPreviewFragment>().guessViewHolder.bind(
                        newGuess
                    )
                    if(newGuess.percentile != null && newGuess.percentile == 1000 && bing == 0) {
                        binding.fragmentContainerView2.getFragment<HeaderFragment>().bingo(newGuess.count);
                        profileStorage.foundWord(newGuess.count);
                        bing = newGuess.count;
                    }
                    if(newGuess.score != null){
                        if (newGuess.count == adapter.currentList.size + 1) {
                            profileStorage.incrAttempts();
                        }

                        wordInput.text.clear();
                    }
                }
            }
        }

        lifecycleScope.launch {
            viewModel.solversStateFlow.collect{ solvers ->
                if(solvers != null)
                    binding.fragmentContainerView2.getFragment<HeaderFragment>().updateSolvers(solvers)
            }
        }

        wordInput.setOnEditorActionListener(TextView.OnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE || binding.validateWord.callOnClick()){

                validateWord()
                return@OnEditorActionListener true;
            }
            false;
        });

    }

    fun validateWord(){
        MediaPlayer.create(context, R.raw.guess).start()
        val text = binding.wordInput.text.toString();
        viewModel.add(text);
    }

    fun clear(){
        val slurp_sound = MediaPlayer.create(context, R.raw.slurp);
        slurp_sound.start()
        viewModel.clear();
        bing = 0;
        binding.fragmentContainerView2.getFragment<HeaderFragment>().clearBingo()
    }

}
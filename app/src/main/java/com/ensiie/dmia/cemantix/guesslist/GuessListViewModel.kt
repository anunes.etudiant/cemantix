package com.ensiie.dmia.cemantix.guesslist

import android.util.Log
import android.widget.Toast
import androidx.compose.ui.text.toLowerCase
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ensiie.dmia.cemantix.data.Api
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class GuessListViewModel : ViewModel() {
    private val webService = Api.scoreWebService

    public val guessStateFlow = MutableStateFlow<List<Guess>>(emptyList())
    public val lastGuessStateFlow = MutableStateFlow<Guess?>(null)
    public val solversStateFlow = MutableStateFlow<Int?>(null)
    public var onInternetError = {}

    fun refresh() {
    }



    // à compléter plus tard:
    fun add(word: String) {
        val cleanWord = word.filter { !it.isWhitespace() }.lowercase();
        if(cleanWord.isBlank()) return;

        val alreadyGuessed : Guess ? = guessStateFlow.value.find { e -> e.word.equals(cleanWord) };
        if(alreadyGuessed != null){
            lastGuessStateFlow.value = null; //force to update if same word
            lastGuessStateFlow.value = alreadyGuessed;
            return;
        }

        var newGuess = Guess(cleanWord);


        viewModelScope.launch {
            try {
                val response = webService.fetchScore(cleanWord) // Call HTTP (opération longue)
                if (!response.isSuccessful) { // à cette ligne, on a reçu la réponse de l'API
                    Log.e("Network", "Error: ${response.message()}")
                    onInternetError();
                    return@launch
                }
                val fetchedScore = response.body()!!
                if(fetchedScore.score != null) {
                    newGuess = newGuess.copy(score = fetchedScore.score, percentile = fetchedScore.percentile, count = guessStateFlow.value.size + 1)
                    guessStateFlow.value = (guessStateFlow.value + newGuess).sortedByDescending { a -> a.score } // on modifie le flow, ce qui déclenche ses observers
                }

                lastGuessStateFlow.value = newGuess;

                solversStateFlow.value = fetchedScore.solvers;
            }catch(e : java.lang.Exception){
                onInternetError();
                return@launch;
            }

        }
    }


    fun clear(){
        guessStateFlow.value = emptyList();
        lastGuessStateFlow.value = null;
    }

    fun submitGuessList(guesses : List<Guess>){
        guessStateFlow.value = guesses;
    }

    fun edit() {}
    fun remove() {}
}
package com.ensiie.dmia.cemantix.data

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

object Api {

    private val okHttpClientCementix = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .addHeader("Origin", "https://cemantix.certitudes.org")
                .build()
            chain.proceed(newRequest)
        }
        .build()

    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .addInterceptor { chain ->
            val newRequest = chain.request().newBuilder()
                .build()
            chain.proceed(newRequest)
        }
        .build()

    private val jsonSerializer = Json {
        ignoreUnknownKeys = true
        coerceInputValues = true
    }

    private val retrofitCemantix by lazy {
        Retrofit.Builder()
            .baseUrl("https://cemantix.certitudes.org/")
            .client(okHttpClientCementix)
            .addConverterFactory(jsonSerializer.asConverterFactory("application/json".toMediaType()))
            .build()
    }

    private val retrofitProfile by lazy {
        Retrofit.Builder()
            .baseUrl("https://nunesa.iiens.net/")
            .client(okHttpClient)
            .addConverterFactory(jsonSerializer.asConverterFactory("application/json".toMediaType()))
            .build()
    }

    val scoreWebService : ScoreWebService by lazy {
        retrofitCemantix.create(ScoreWebService::class.java)
    }

    val profilWebService : ProfileWebService by lazy {
        retrofitProfile.create(ProfileWebService::class.java)
    }


}
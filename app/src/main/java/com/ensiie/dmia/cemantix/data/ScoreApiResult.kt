package com.ensiie.dmia.cemantix.data

import java.io.Serializable

@kotlinx.serialization.Serializable
data class ScoreApiResult(
    @kotlinx.serialization.SerialName("num")
    val num : Int,

    @kotlinx.serialization.SerialName("solvers")
    val solvers : Int,

    @kotlinx.serialization.SerialName("score")
    val score : Float? = null,

    @kotlinx.serialization.SerialName("percentile")
    val percentile : Int? = null,

    ):Serializable

# Projet DM : Jeu Cemantix

Projet réalisé par **Adrien Nunes** et **Martin Poiroux**
 
---
## Principe du jeu : 
Le but du jeu est de trouver le mot secret en essayant de s’en approcher le plus possible *contextuellement*. Chaque mot se voit attribuer une température dont des valeurs intéressantes sont données en légende à gauche. Si votre mot se trouve dans les 1000 mots les plus proches, un indice de progression gradué de 1 à 1000 ‰ apparaîtra.

La proximité d’un mot n’est pas orthographique  mais sémantique ou contextuelle. Elle est évaluée non pas à l’aide d’un dictionnaire, mais d’une base de données de textes de plus d’un milliard de mots à partir de laquelle on a calculé une “distance” relative entre chaque mot. Deux mots proches dans un tel champ lexical ne sont pas nécessairement synonymes. Par exemple, il se peut qu’un adjectif et son contraire soient considérés comme proches car ils peuvent qualifier la même chose.

Les mots sont les mêmes que sur le site original [cemantix.certitudes.org](https://cemantix.certitudes.org/), l'application utilise directement leur API. Il y a un nouveau mot pris au hasard chaque jour à minuit heure française.

---

##  Utilisation de notre application


<img src="screenshots/full_app.png" width="300"  />


Notre application est composée d'un **header**, comprenant : 
- Le logo de notre application, qui redirige vers le site original [cemantix.certitudes.org](https://cemantix.certitudes.org/)
- Le numéro du jouer ainsi que le nombre de personnes ayant trouvé le mot du jour
- L'image de profil de l'utilisateur, qui redirige vers la vue qui permet d'éditer le profil et de voir les statistiques de l'utilisateur.


On retrouve ensuite un input pour saisir le mot, le mot peut être envoyé avec la touche "entrée" ou avec le bouton vert à droite. Le bouton rouge lui permet de vider la liste des essais.

Le dernier élément est l'ensemble des mots essayés par l'utilisateur, tout en haut, dans le bandeau bleu, on retrouve le dernier mot essayé par l'utilisateur, en dessous la liste des mots triés par "proximité" avec le mot secret.

Pour chaque mot on a : 
- Le numéro de l'essai
- le mot
- La température du mot (proximité avec le mot secret)
- Un indice de progression si le mot se trouve dans les 1000 mots les plus proches.


<img src="screenshots/win.png" width="300"  />

Lorsque l'utilisateur trouve le mot secret, l'en-tête passe en vert avec un message de félicitation.


---
## Edition du profil & stats

En cliquant en haut à droite, sur l'image du profil il est possible d'éditer le profil et de voir les statistiques.

<img src="screenshots/edit_profil.png" width="300"  />

Il est possible de modifier son pseudo, de prendre une photo via l'appareil ou bien choisir une photo existante.

On retrouve les statistiques de l'utilisteurs : 
- Nombre de mots secrets trouvés
- Le nombre total d'essais
- Le nombre d'essais minimum pour deviner un mot secret


<img src="screenshots/after_edit.png" width="300"  />

Lorsque l'on valide le profil, l'image à bien changé dans l'application.

---

## Autre

Le profil et la liste des essais sont enregistrés en local, donc si l'utilisateur ferme l'application, ou redémarre son téléphonne il pourra retrouver ses statistiques et les mots qu'il a essayés le jour même. Si le jour change, alors la liste des mots essayés sera remise à 0.

Notre application est dotée d'une sonothèque amusante :'D